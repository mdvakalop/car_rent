/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import static db.GiftToCustomers.DB_URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author gxx
 */
public class VehiclesAtAllStoreHP extends javax.swing.JFrame {

   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/team4444";
   //  Database credentials
   static final String USER = "root";
   static final String PASS = "kokos"; 
   String[] columns=null;
   Object [][] data=null;

    /**
     * Creates new form VehiclesAtAllStoreHP
     */
    public VehiclesAtAllStoreHP() {
        initComponents();
        try { 
            Connection conn = null;
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //System.out.println("Connected to a selected database...");
            String query ="select v.LicensePlate, v.Model, v.HorsePower,  s.City " +
                    "from Vehicle v, store s " +
                    "where v.StoreId = s.StoreId " +
                    "order by s.StoreId, v.HorsePower DESC ";
            Statement st = conn.createStatement();
            ResultSet rsQuery = st.executeQuery(query);
            String rowsQuery="select Count(Distinct v.LicensePlate ) as count " +
                  "from Vehicle v, store s  " +
                  "where v.StoreId = s.StoreId " +
                  "order by s.StoreId, v.HorsePower DESC ";
           Statement stRows = conn.createStatement();
           ResultSet rsRowsQuery = stRows.executeQuery(rowsQuery);
           rsRowsQuery.next();
           int rows = rsRowsQuery.getInt("count");
           
           if (rows == 0) 
               JOptionPane.showMessageDialog(null,"Δεν υπάρχουν οχήματα σε κανένα κατάστημα");
           else {
                data = new String [rows][4];
                int i=0;
                columns = new String[]{"License Plate","Model","Horse Power","City"};

                while(rsQuery.next()) {
                   data[i][0] = rsQuery.getString(1);
                   data[i][1] = rsQuery.getString(2);
                   data[i][2] = rsQuery.getString(3);
                   data[i][3] = rsQuery.getString(4);
                   //data[i][4] = rsQuery.getString(5);
                   i++;
                } 
                JTable Jtable1 = new  JTable(data,columns);
                jScrollPane1.setViewportView(Jtable1);
            }    
        }catch(SQLException se){
          //Handle errors for JDBC
          se.printStackTrace();
        }catch(Exception e){      
          e.printStackTrace();
        }         
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Οχήματα ταξινομημένα  ως προς την ιπποδύναμη ανά κατάστημα");
        setLocation(new java.awt.Point(350, 150));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 804, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VehiclesAtAllStoreHP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VehiclesAtAllStoreHP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VehiclesAtAllStoreHP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VehiclesAtAllStoreHP.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VehiclesAtAllStoreHP().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
